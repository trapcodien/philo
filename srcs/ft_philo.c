/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_philo.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/05 19:48:43 by garm              #+#    #+#             */
/*   Updated: 2014/05/11 14:37:22 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_philo.h"

void	ft_init_philosophers(t_env *e)
{
	int			i;

	i = 0;
	while (i < NB_PHILOS)
	{
		e->philos[i].life = MAX_LIFE;
		e->philos[i].id = i;
		e->philos[i].state = STATE_REST;
		e->philos[i].left = &(e->sticks[i]);
		e->philos[i].right = &(e->sticks[(i + 1) % 7]);
		i++;
	}
}

void	ft_init_sticks(t_env *e)
{
	int			i;

	i = 0;
	while (i < NB_PHILOS)
	{
		e->sticks[i].state = STICK_DIRTY;
		pthread_mutex_init(&(e->sticks[i].lock), NULL);
		i++;
	}
}

int		ft_key_hook(int keycode, t_env *e)
{
	int		i;

	if (keycode == KEY_ESC)
	{
		i = 0;
		while (i < 7)
		{
			pthread_detach(e->threads[i]);
			i++;
		}
		ft_img_destroy(e->screen);
		exit(0);
	}
	(void)e;
	return (0);
}

int		ft_loop_hook(t_env *e)
{
	if (!e->someone_is_dead && !e->end)
	{
		ft_refresh(e);
		if (e->nbseconds >= 0)
		{
			ft_putstr("Time out in ");
			ft_putnbr(TIMEOUT - e->nbseconds);
			ft_putendl(" seconds(s)");
		}
		usleep(USECOND);
		e->nbseconds++;
		e->someone_is_dead = ft_refresh_lifes(e);
		if (e->someone_is_dead)
			ft_putendl("A philosopher is dead...");
		if (e->nbseconds >= TIMEOUT)
		{
			if (!e->someone_is_dead)
			{
				e->end = 1;
				ft_putendl("Now, it is time... To DAAAAAAAANCE!!!");
			}
		}
	}
	return (0);
}

int		main(void)
{
	t_env		e;

	ft_putendl("\n\n\n\t-------- LEGEND --------");
	ft_putendl("\t|   RED   = WAIT       |");
	ft_putendl("\t|   BLUE  = THINK      |");
	ft_putendl("\t|   GREEN = EAT        |");
	ft_putendl("\t------------------------\n\n");
	ft_init_philosophers(&e);
	ft_init_sticks(&e);
	e.mlx = mlx_init();
	e.width = WIN_WIDTH;
	e.height = WIN_HEIGHT;
	e.win = mlx_new_window(e.mlx, e.width, e.height, WIN_TITLE);
	e.screen = ft_img_create(&e, e.width, e.height);
	e.someone_is_dead = 0;
	e.nbseconds = -1;
	e.end = 0;
	mlx_key_hook(e.win, ft_key_hook, &e);
	mlx_loop_hook(e.mlx, ft_loop_hook, &e);
	ft_start_threads(&e);
	mlx_loop(e.mlx);
	return (0);
}
