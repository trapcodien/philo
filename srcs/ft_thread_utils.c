/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_thread_utils.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/11 05:49:07 by garm              #+#    #+#             */
/*   Updated: 2014/05/11 13:21:56 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_philo.h"

int		get_neighbor_id(char side, int id)
{
	if (side == SIDE_LEFT)
	{
		id--;
		if (id < 0)
			id = 6;
		return (id);
	}
	else
		return ((id + 1) % 7);
}

char	get_side_available_stick(t_philo *philo)
{
	pthread_mutex_t		*left;
	pthread_mutex_t		*right;

	left = &(philo->left->lock);
	right = &(philo->right->lock);
	if (pthread_mutex_trylock(left) == 0)
	{
		pthread_mutex_unlock(left);
		return (SIDE_LEFT);
	}
	if (pthread_mutex_trylock(right) == 0)
	{
		pthread_mutex_unlock(right);
		return (SIDE_RIGHT);
	}
	return (-1);
}

int		trylock_stick(t_data *data, t_philo *philo, t_stick *stick, char side)
{
	pthread_mutex_t		*lock;
	t_philo				*other;

	other = &(data->e->philos[get_neighbor_id(side, data->id)]);
	lock = &(stick->lock);
	if (pthread_mutex_trylock(lock) == 0)
	{
		if (philo->life == other->life)
		{
			if (philo->id > other->id)
				return (1);
			else
				pthread_mutex_unlock(lock);
		}
		else if (philo->life < other->life)
			return (1);
		else
			pthread_mutex_unlock(lock);
	}
	return (0);
}
