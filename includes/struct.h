/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   struct.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/11 08:15:16 by garm              #+#    #+#             */
/*   Updated: 2014/05/11 13:38:32 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef STRUCT_H
# define STRUCT_H

typedef struct				s_philo
{
	int						life;
	int						id;
	int						state;
	struct s_stick			*left;
	struct s_stick			*right;
}							t_philo;

typedef struct				s_stick
{
	int						state;
	pthread_mutex_t			lock;
}							t_stick;

typedef struct				s_data
{
	int						id;
	struct s_env			*e;
}							t_data;

typedef struct				s_env
{
	void					*mlx;
	void					*win;
	struct s_img			*screen;
	int						width;
	int						height;
	struct s_data			data[7];
	struct s_philo			philos[7];
	struct s_stick			sticks[7];
	pthread_t				threads[7];
	int						someone_is_dead;
	int						nbseconds;
	int						end;
}							t_env;

typedef struct				s_img
{
	t_env					*e;
	void					*ptr;
	char					*data;
	int						bpp;
	int						sizel;
	int						endian;
	int						w;
	int						h;
}							t_img;

#endif
