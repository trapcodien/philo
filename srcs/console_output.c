/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   console_output.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/11 13:51:30 by garm              #+#    #+#             */
/*   Updated: 2014/05/11 14:31:26 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_philo.h"

void	ft_putnbr(int num)
{
	if (num < 0)
	{
		num = num * -1;
		write(1, "-", 1);
	}
	if (num > 9)
	{
		ft_putnbr(num / 10);
		ft_putnbr(num % 10);
	}
	else
	{
		num = num + 48;
		write(1, &num, 1);
	}
}

void	ft_putstr(char *str)
{
	int		i;

	i = 0;
	while (str && str[i])
		i++;
	write(1, str, i);
}

void	ft_putendl(char *str)
{
	ft_putstr(str);
	ft_putstr("\n");
}
