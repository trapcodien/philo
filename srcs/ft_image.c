/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   image.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/09 13:50:59 by garm              #+#    #+#             */
/*   Updated: 2014/05/11 06:23:20 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_philo.h"

t_img	*ft_img_create(t_env *e, int w, int h)
{
	t_img	*i;

	i = (t_img *)malloc(sizeof(t_img));
	i->ptr = mlx_new_image(e->mlx, w, h);
	i->data = mlx_get_data_addr(i->ptr, &(i->bpp), &(i->sizel), &(i->endian));
	i->e = e;
	i->w = w;
	i->h = h;
	return (i);
}

void	ft_img_pixel_put(t_img *img, int x, int y, int color)
{
	char			*data;
	unsigned int	*pixel;

	data = img->data;
	pixel = (unsigned int *)((data + (y * img->sizel) + ((img->bpp / 8) * x)));
	*pixel = mlx_get_color_value(img->e->mlx, color);
}

void	ft_img_reset(t_img *img)
{
	int		i;
	int		j;

	i = 0;
	while (i < img->w)
	{
		j = 0;
		while (j < img->h)
		{
			ft_img_pixel_put(img, i, j, COLOR(0, 0, 0));
			j++;
		}
		i++;
	}
}

void	ft_img_display(t_img *img)
{
	mlx_put_image_to_window(img->e->mlx, img->e->win, img->ptr, 0, 0);
}

void	ft_img_destroy(t_img *img)
{
	t_env	*e;

	e = img->e;
	mlx_destroy_image(e->mlx, img->ptr);
	free((void *)img);
}
