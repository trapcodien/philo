# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: garm <garm@student.42.fr>                  +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2013/08/26 23:49:31 by garm              #+#    #+#              #
##   Updated: 2014/05/09 18:00:32 by garm             ###   ########.fr       ##
#                                                                              #
# **************************************************************************** #

CC = gcc

NAME = philo
LIBS = -lmlx -lXext -lX11 -lpthread
FTLIBS = libft.a

LIB_DIR = libft
SOURCES_DIR = srcs
INCLUDES_DIR = includes

ifeq ($(DEBUG), 1)
	FLAGS = -g -Wall -Wextra -pedantic
	CC = cc
else
	FLAGS = -Wall -Werror -Wextra -ansi -pedantic -std=c89
endif

ifeq ($(STRICT), 1)
	FLAGS += -fstack-protector-all -Wshadow -Wunreachable-code \
			  -Wstack-protector -pedantic-errors -O0 -W -Wundef -fno-common \
			  -Wfatal-errors -Wstrict-prototypes -Wmissing-prototypes \
			  -Wwrite-strings -Wunknown-pragmas -Wdeclaration-after-statement \
			  -Wold-style-definition -Wmissing-field-initializers \
			  -Wpointer-arith -Wnested-externs -Wstrict-overflow=5 -fno-common \
			  -Wno-missing-field-initializers -Wswitch-default -Wswitch-enum \
			  -Wbad-function-cast -Wredundant-decls -fno-omit-frame-pointer \
			  -Wfloat-equal
endif

CFLAGS =  $(FLAGS) -I $(INCLUDES_DIR)
LDFLAGS = $(LIBS) -L /usr/X11/lib

DEPENDENCIES = \
			   $(INCLUDES_DIR)/ft_philo.h

SOURCES = \
		  $(SOURCES_DIR)/console_output.c \
		  $(SOURCES_DIR)/ft_philo.c \
		  $(SOURCES_DIR)/ft_image.c \
		  $(SOURCES_DIR)/ft_refresh.c \
		  $(SOURCES_DIR)/ft_thread.c \
		  $(SOURCES_DIR)/ft_thread_utils.c

OBJS = $(SOURCES:.c=.o)

all: $(NAME)

%.o: %.c $(DEPENDENCIES)
	$(CC) -c $< -o $@ $(CFLAGS)

$(NAME): $(OBJS)
	@echo Creating $(NAME)...
	@$(CC) -o $(NAME) $(OBJS) $(LDFLAGS)

test:
	@$(MAKE)
	@echo
	@echo ------- Program Start -------
	@echo
	@./$(NAME)

clean:
	@rm -f $(OBJS)
	@echo Deleting $(NAME) OBJECTS files...

fclean: clean
	@rm -f $(NAME)
	@echo Deleting $(NAME)...

re: fclean all

.PHONY: clean fclean re all lib test

