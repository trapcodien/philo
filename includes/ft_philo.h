/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_philo.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/05 19:49:21 by garm              #+#    #+#             */
/*   Updated: 2014/05/11 14:38:39 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PHILO_H
# define FT_PHILO_H

# include <unistd.h>
# include <stdlib.h>
# include <time.h>
# include <pthread.h>
# include <mlx.h>
# include "struct.h"

# define MAX_LIFE 12
# define REST_T 1
# define THINK_T 2
# define EAT_T 3
# define TIMEOUT 42

# define USECOND 1000000
# define COLOR(R,G,B) ((R) * 65536 + (G) * 256 + (B))
# define KEY_ESC 65307

# define WIN_WIDTH 550
# define WIN_HEIGHT 600
# define WIN_TITLE "Le diner des philosophes mangeur de riz"

# define NB_PHILOS 7

# define STATE_REST 0
# define STATE_THINK 1
# define STATE_EAT 2

# define SIDE_LEFT 0
# define SIDE_RIGHT 1

# define STICK_CLEAN 0
# define STICK_DIRTY 1

/*
** ft_image.c
*/
t_img	*ft_img_create(t_env *e, int w, int h);
void	ft_img_pixel_put(t_img *img, int x, int y, int color);
void	ft_img_reset(t_img *img);
void	ft_img_display(t_img *img);
void	ft_img_destroy(t_img *img);

/*
** ft_refresh.c
*/
void	ft_state_to_color(t_philo *philo, int *color, int *color2);
void	ft_draw_philo(t_env *e, int x, int y, int id);
void	ft_refresh(t_env *e);
int		ft_refresh_lifes(t_env *e);

/*
** ft_thread.c
*/
void	ft_start_threads(t_env *e);
void	ft_rest(t_data *data, t_philo *philo);
void	ft_think(t_data *data, t_philo *philo);
void	ft_philosophers_init(t_philo *philo);
void	*ft_philo(void *ptr);

/*
** ft_thread_utils.c
*/
int		get_neighbor_id(char side, int id);
char	get_side_available_stick(t_philo *philo);
int		trylock_stick(t_data *data, t_philo *philo, t_stick *stick, char side);

/*
** console_output.c
*/
void	ft_putnbr(int i);
void	ft_putstr(char *str);
void	ft_putendl(char *str);

#endif
