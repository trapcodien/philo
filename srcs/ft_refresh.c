/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_refresh.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/10 22:39:03 by garm              #+#    #+#             */
/*   Updated: 2014/05/11 13:42:20 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_philo.h"

void	ft_state_to_color(t_philo *philo, int *color, int *color2)
{
	int		state;

	state = philo->state;
	if (state == STATE_REST)
	{
		*color = COLOR(255, 0, 0);
		*color2 = COLOR(55, 0, 0);
	}
	else if (state == STATE_THINK)
	{
		*color = COLOR(0, 0, 255);
		*color2 = COLOR(0, 0, 55);
	}
	else if (state == STATE_EAT)
	{
		*color = COLOR(0, 255, 0);
		*color2 = COLOR(0, 55, 0);
		philo->life = MAX_LIFE;
	}
}

void	ft_draw_philo(t_env *e, int x, int y, int id)
{
	int		i;
	int		j;
	int		color;
	int		color2;

	ft_state_to_color(&(e->philos[id]), &color, &color2);
	i = 0;
	while (i < 50)
	{
		j = 0;
		while (j < 50)
		{
			if (j >= (50 - ((e->philos[id].life * 100) / MAX_LIFE) / 2))
				ft_img_pixel_put(e->screen, x + i, y + j, color);
			else
				ft_img_pixel_put(e->screen, x + i, y + j, color2);
			j++;
		}
		i++;
	}
}

void	ft_refresh(t_env *e)
{
	ft_img_reset(e->screen);
	ft_draw_philo(e, 250, 100, e->philos[6].id);
	ft_draw_philo(e, 350, 200, e->philos[5].id);
	ft_draw_philo(e, 350, 300, e->philos[4].id);
	ft_draw_philo(e, 300, 400, e->philos[3].id);
	ft_draw_philo(e, 200, 400, e->philos[2].id);
	ft_draw_philo(e, 150, 300, e->philos[1].id);
	ft_draw_philo(e, 150, 200, e->philos[0].id);
	ft_img_display(e->screen);
}

int		ft_refresh_lifes(t_env *e)
{
	if (e->philos[0].life <= 0)
		return (1);
	if (e->philos[1].life <= 0)
		return (1);
	if (e->philos[2].life <= 0)
		return (1);
	if (e->philos[3].life <= 0)
		return (1);
	if (e->philos[4].life <= 0)
		return (1);
	if (e->philos[5].life <= 0)
		return (1);
	if (e->philos[6].life <= 0)
		return (1);
	e->philos[0].life--;
	e->philos[1].life--;
	e->philos[2].life--;
	e->philos[3].life--;
	e->philos[4].life--;
	e->philos[5].life--;
	e->philos[6].life--;
	return (0);
}
