/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_thread.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/10 22:46:55 by garm              #+#    #+#             */
/*   Updated: 2014/05/11 14:08:04 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_philo.h"

void	ft_start_threads(t_env *e)
{
	int		i;

	i = 0;
	while (i < 7)
	{
		e->data[i].id = i;
		e->data[i].e = e;
		pthread_create(&(e->threads[i]), NULL, ft_philo, &e->data[i]);
		i++;
	}
}

void	ft_rest(t_data *data, t_philo *philo)
{
	usleep(USECOND * REST_T);
	if (trylock_stick(data, philo, philo->left, SIDE_LEFT))
		philo->state = philo->state + 1;
	if (trylock_stick(data, philo, philo->right, SIDE_RIGHT))
		philo->state++;
}

void	ft_think(t_data *data, t_philo *philo)
{
	t_philo		*p;
	t_stick		*available;
	t_stick		*unavailable;
	char		side;

	usleep(USECOND * THINK_T);
	available = philo->right;
	side = SIDE_RIGHT;
	unavailable = philo->left;
	p = &(data->e->philos[get_neighbor_id(SIDE_LEFT, data->id)]);
	if (get_side_available_stick(philo) == SIDE_LEFT)
	{
		available = philo->left;
		side = SIDE_LEFT;
		unavailable = philo->right;
		p = &(data->e->philos[get_neighbor_id(SIDE_RIGHT, data->id)]);
	}
	if (trylock_stick(data, philo, available, side))
		philo->state++;
	if (p->life < philo->life || (p->life == philo->life && philo->id < p->id))
	{
		philo->state--;
		pthread_mutex_unlock(&(unavailable->lock));
	}
}

void	ft_philosophers_init(t_philo *philo)
{
	if (philo->id == 0 || philo->id == 2 || philo->id == 4)
	{
		philo->state = STATE_EAT;
		pthread_mutex_lock(&(philo->left->lock));
		pthread_mutex_lock(&(philo->right->lock));
	}
	else if (philo->id == 5)
	{
		philo->state = STATE_THINK;
		pthread_mutex_lock(&(philo->right->lock));
	}
}

void	*ft_philo(void *ptr)
{
	t_data	*data;
	t_philo	*philo;

	data = (t_data *)ptr;
	philo = &(data->e->philos[data->id]);
	ft_philosophers_init(philo);
	usleep(philo->id * 10000);
	while (42)
	{
		if (philo->state == STATE_REST)
			ft_rest(data, philo);
		else if (philo->state == STATE_THINK)
			ft_think(data, philo);
		else if (philo->state == STATE_EAT)
		{
			usleep(USECOND * EAT_T);
			pthread_mutex_unlock(&philo->left->lock);
			pthread_mutex_unlock(&philo->right->lock);
			philo->state = 0;
		}
	}
	return (NULL);
}
